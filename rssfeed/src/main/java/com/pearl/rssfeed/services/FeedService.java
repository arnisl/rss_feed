package com.pearl.rssfeed.services;

import java.util.List;

import com.pearl.rssfeed.models.Feed;

/**
 * Feed entity access service interface
 */
public interface FeedService {

	int addFeed(String name, String url);
	
	Feed getFeedById(int feedId);
	
	List<Feed> getFeeds();
	
	String getErrorMessage();
}
