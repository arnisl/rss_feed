package com.pearl.rssfeed.services;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.time.OffsetDateTime;
import java.time.format.DateTimeFormatter;

import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.Characters;
import javax.xml.stream.events.XMLEvent;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.pearl.rssfeed.dao.FeedDao;
import com.pearl.rssfeed.dao.FeedItemDao;
import com.pearl.rssfeed.models.Feed;
import com.pearl.rssfeed.models.FeedItem;

import lombok.extern.slf4j.Slf4j;

/**
 * RSS XML feed data parser class
 */
@Slf4j
@Service
public class RssFeedParser {
	
	private static final String TITLE = "title";
	private static final String DESCRIPTION = "description";
	private static final String LINK = "link";
	private static final String ITEM = "item";
    private static final String PUB_DATE = "pubDate";
    private static final String LAST_BUILD_DATE = "lastBuildDate";
    
	@Autowired
	private FeedItemDao feedItemDao;
	
	@Autowired
	private FeedDao feedDao;

	public void readFeedItems(Feed feed) throws XMLStreamException {
		boolean isFeedHeader = true;
		// Set header values intial to the empty string
		String description = "";
		String title = "";
		String link = "";
		String pubdate = "";
		String lastBuildDate = "";

		// First create a new XMLInputFactory
		XMLInputFactory inputFactory = XMLInputFactory.newInstance();
		
		// Setup a new eventReader
		InputStream in = read(feed.getUrl());
				
		XMLEventReader eventReader = inputFactory.createXMLEventReader(in);
	
		// Read the XML document
		while (eventReader.hasNext()) {
			XMLEvent event = eventReader.nextEvent();
			
			if (event.isStartElement()) {
				String localPart = event.asStartElement().getName().getLocalPart();
				switch (localPart) {
				case ITEM:
					if (isFeedHeader) {
						isFeedHeader = false;
						
						if (feed.getTitle() == null) {
							feed.setTitle(title);
						}
						feed.setLastUpdate(stringToOffsetDateTime(lastBuildDate));
					}
					event = eventReader.nextEvent();
					break;
				case TITLE:
					title = getCharacterData(event, eventReader);
					break;
				case DESCRIPTION:
					description = getCharacterData(event, eventReader);
					break;
				case LINK:
					link = getCharacterData(event, eventReader);
					break;
				case PUB_DATE:
					pubdate = getCharacterData(event, eventReader);
					break;
				case LAST_BUILD_DATE:
					lastBuildDate = getCharacterData(event, eventReader);
					break;
				}
			} else if (event.isEndElement()) {
				if (event.asEndElement().getName().getLocalPart() == (ITEM)) {
					FeedItem item = new FeedItem();
					item.setDescription(description);
					try {
						item.setLink(new URL(link));
					} catch (MalformedURLException e) {
						log.error("Feed item URL exception: {}", e.getMessage());
					}
					item.setTitle(title);
					item.setPublished(stringToOffsetDateTime(pubdate));
					item.setFeed(feed);
									
					feedItemDao.save(item);
					
					feed.getItems().add(item);
					
					event = eventReader.nextEvent();
					continue;
				}
			}
		}
		
		feedDao.save(feed);		
	}
	
	private String getCharacterData(XMLEvent event, XMLEventReader eventReader) throws XMLStreamException {
        String result = "";
        event = eventReader.nextEvent();
        if (event instanceof Characters) {
            result = event.asCharacters().getData();
        }
        return result;
    }

    private InputStream read(URL url) {
    	try {
    		URLConnection conn = url.openConnection();
    		
        	int rc = ((HttpURLConnection) conn).getResponseCode();
        	
        	return conn.getInputStream();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
    
	private static OffsetDateTime stringToOffsetDateTime(String dateStr) {
		OffsetDateTime dateTime = OffsetDateTime.from(DateTimeFormatter.RFC_1123_DATE_TIME.parse(dateStr));
		return dateTime;
	}
}
