package com.pearl.rssfeed.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import com.pearl.rssfeed.dao.FeedItemDao;
import com.pearl.rssfeed.models.Feed;
import com.pearl.rssfeed.models.FeedItem;

/**
 * Feed item entity access service implementation
 */
@Service
public class FeedItemServiceImpl implements FeedItemService {

	@Autowired
	private FeedItemDao feedItemDao;
	
	@Override
	public List<FeedItem> getRecentItems(Feed feed, int maxItemCount) {
		return feedItemDao.getRecentItems(feed, PageRequest.of(0, maxItemCount));
	}
}
