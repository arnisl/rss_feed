package com.pearl.rssfeed.services;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;

import javax.xml.stream.XMLStreamException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.pearl.rssfeed.dao.FeedDao;
import com.pearl.rssfeed.models.Feed;

import lombok.extern.slf4j.Slf4j;

/**
 * Feed entity access service implementation
 */
@Service
@Slf4j
public class FeedServiceImpl implements FeedService {

	@Autowired
	private FeedDao feedDao;
	
	@Autowired
	private RssFeedParser feedParser;
	
	private String errorMessage = "";

	@Override
	public int addFeed(String name, String url) {
		errorMessage = "";
		
		Feed feed = new Feed();
		feed.setName(name);
		try {
			feed.setUrl(new URL(url));
		} catch (MalformedURLException e) {
			throw new RuntimeException(e);
		}
		
		feedDao.save(feed);
				
		try {
			feedParser.readFeedItems(feed);
		} catch (XMLStreamException e) {
			errorMessage = "Feed XML parsing gerror";
			log.error(errorMessage + ": " + e.getMessage());
		}
		
		return feed.getId();
	}
	
	@Override
	public String getErrorMessage() {
		return errorMessage;
	}
	
	@Override
	public Feed getFeedById(int feedId) {
		return feedDao.findById(feedId).orElse(null);
	}

	@Override
	public List<Feed> getFeeds() {
		return feedDao.findAll();
	}
}
