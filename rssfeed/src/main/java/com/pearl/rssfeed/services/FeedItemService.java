package com.pearl.rssfeed.services;

import java.util.List;

import com.pearl.rssfeed.models.Feed;
import com.pearl.rssfeed.models.FeedItem;

/**
 * Feed item entity access service interface
 */
public interface FeedItemService {

	List<FeedItem> getRecentItems(Feed feed, int maxItemCount);
}
