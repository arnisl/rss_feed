package com.pearl.rssfeed.models;

import java.net.URL;
import java.time.OffsetDateTime;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import lombok.Data;

/**
 * Specifies the ORM entity class for feeds table.
 */
@Entity
@Table(name = "feeds")
@Data
public class Feed {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	
	@Column
	private URL url;
	
	@Column
	private String title;
	
	@Column(name="feed_name")
	private String name;
	
	@Column(name="last_update")
	private OffsetDateTime lastUpdate;
	
	@OneToMany(cascade = CascadeType.ALL, mappedBy = "feed")
	private List<FeedItem> items = new ArrayList<>();
}
