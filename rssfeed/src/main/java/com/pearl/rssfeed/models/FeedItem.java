package com.pearl.rssfeed.models;

import java.net.URL;
import java.time.OffsetDateTime;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.Type;

import lombok.Data;

/**
 * Specifies the ORM entity class for items table.
 */
@Entity
@Table(name = "items")
@Data
public class FeedItem {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	
	@Column
	private String title;
	
	@Column
	private URL link;
	
	@Column
	@Type(type="text")
	private String description;
	
	@Basic
	private OffsetDateTime published;
	
	@ManyToOne
	@JoinColumn(name = "feed_id")
	private Feed feed;
}
