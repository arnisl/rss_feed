package com.pearl.rssfeed;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * Application class to run the embedded web server
 */
@SpringBootApplication
public class RssFeedApplication {

	public static void main(String[] args) {
        SpringApplication.run(RssFeedApplication.class, args);
    }
}
