package com.pearl.rssfeed.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.pearl.rssfeed.dto.FeedDto;
import com.pearl.rssfeed.models.Feed;
import com.pearl.rssfeed.services.FeedItemService;
import com.pearl.rssfeed.services.FeedService;
import com.pearl.rssfeed.validators.FeedFormValidator;


/**
 * Implementation of RSS feed web controller class
 */
@Controller
public class RssFeedController {
	
	private static final int MAX_RECENT_ITEM_COUNT = 5;
	
	@Autowired
	FeedService feedService;
	
	@Autowired
	FeedItemService feedItemsService;
	
	@Autowired
	FeedFormValidator feedFormValidator;
	
	@InitBinder
	protected void initBinder(WebDataBinder binder) {
		binder.setValidator(feedFormValidator);
	}

	@GetMapping("/")
	public String index() {
		return "redirect:/feeds";
	}
	
	@GetMapping("feeds")
	public ModelAndView feeds() {
		ModelAndView mv = new ModelAndView("feeds/list", "feeds", feedService.getFeeds());
		return mv;
	}
	
	@GetMapping("/feeds/{id}")
	public ModelAndView feedById(@PathVariable("id") int feedId, ModelMap model) {
		Feed feed = feedService.getFeedById(feedId);
		
		model.addAttribute("itemCount", feed.getItems().size());
		
		model.addAttribute("recentItems", feedItemsService.getRecentItems(feed, MAX_RECENT_ITEM_COUNT));
		
		return new ModelAndView("feeds/show", "feed", new FeedDto(feed));
	}
	
	@RequestMapping(value = "feeds/add", method = RequestMethod.GET)
	public ModelAndView showForm() {
		 return new ModelAndView("feeds/edit", "feed", new FeedDto());
	}
	
	@RequestMapping(value = "feeds", method = RequestMethod.POST)
    public String submit(@ModelAttribute("feed") @Validated FeedDto feed, 
    		BindingResult result, ModelMap model, final RedirectAttributes redirectAttributes) { 
		
		if (result.hasErrors()) {
			return "feeds/edit";
		} 
		
		int feedId = feedService.addFeed(feed.getName(), feed.getUrl());
        
        if (feedService.getErrorMessage().isEmpty()) {
        	redirectAttributes.addFlashAttribute("css", "success");
        	redirectAttributes.addFlashAttribute("msg", "Feed added successfully!");
        } else {
        	redirectAttributes.addFlashAttribute("css", "danger");
        	redirectAttributes.addFlashAttribute("msg", feedService.getErrorMessage());
        }
               
        return "redirect:/feeds/" + feedId;
    }
}
