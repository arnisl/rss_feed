package com.pearl.rssfeed.dao;

import java.util.List;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.pearl.rssfeed.models.Feed;
import com.pearl.rssfeed.models.FeedItem;

/**
 * Data Access Object (DAO) class encapsulating storage, retrieval, and search of feed item entities.
 * 
 * <p>DAO Design Pattern is used to separate the data persistence logic in a separate layer (Separation of Logic). 
 * The class implements also the principle of a "Repository", originally defined by Domain-Driven Design.
 */
@Repository
public interface FeedItemDao extends JpaRepository<FeedItem, Integer> {
	
	@Query("SELECT fi FROM FeedItem fi WHERE fi.feed = :feed ORDER BY fi.published DESC")
    List<FeedItem> getRecentItems(Feed feed, Pageable pageable);
}
