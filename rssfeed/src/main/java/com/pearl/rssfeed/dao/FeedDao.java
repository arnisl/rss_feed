package com.pearl.rssfeed.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.pearl.rssfeed.models.Feed;

/**
 * Data Access Object (DAO) class encapsulating storage, retrieval, and search of feed entities.
 * 
 * <p>DAO Design Pattern is used to separate the data persistence logic in a separate layer (Separation of Logic). 
 * The class implements also the principle of a "Repository", originally defined by Domain-Driven Design.
 */
@Repository
public interface FeedDao extends JpaRepository<Feed, Integer> {
}
