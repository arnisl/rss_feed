package com.pearl.rssfeed.validators;

import org.apache.commons.validator.routines.UrlValidator;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import com.pearl.rssfeed.dto.FeedDto;


/**
 * Validator class for feed forms
 */
@Component
public class FeedFormValidator implements Validator {
	
	private UrlValidator urlValidator = new UrlValidator();
	
	@Override
	public boolean supports(Class<?> clazz) {
		return FeedDto.class.equals(clazz);
	}

	@Override
	public void validate(Object target, Errors errors) {
		FeedDto feed = (FeedDto) target;

		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "name", "NotEmpty.feed.name");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "url", "NotEmpty.feed.url");
		
		if (!urlValidator.isValid(feed.getUrl()) ){
			errors.rejectValue("url", "feed.url.invalid");
		}
	}
}
