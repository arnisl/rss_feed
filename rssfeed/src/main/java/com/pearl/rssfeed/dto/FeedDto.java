package com.pearl.rssfeed.dto;

import java.time.OffsetDateTime;

import com.pearl.rssfeed.models.Feed;

import lombok.Data;

/**
 * Data Transfer Object (DTO) class to exchange feed data between the ORM and web view layers.
 * 
 * <p>A DTO is an object that carries data between processes. 
 * The difference between DTO and DAO is that a DTO does not have any behavior except for storage, retrieval, serialization 
 * and deserialization of its own data.
 */
@Data
public class FeedDto {
	
	public FeedDto() {
	}
	
	public FeedDto(Feed feed) {
		this.name = feed.getName();
		this.url = feed.getUrl().toString();
		this.lastUpdate = feed.getLastUpdate();
	}

	private String name;
	
	private String url;
	
	private OffsetDateTime lastUpdate;
}
