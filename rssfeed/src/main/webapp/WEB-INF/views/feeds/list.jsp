<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<!DOCTYPE html>
<html>

<jsp:include page="../fragments/header.jsp" />

<body>
	<div class="container">
		<h1>XML RSS Feed</h1>
		<c:if test="${not empty feeds}">
			<h2>Please find the list of all available feeds</h2>
			<table class="table table-striped">
				<c:forEach var="feed" items="${feeds}">
					<tr>
						<td>
							<a href='<c:url value="feeds/${feed.id}"/>'>${feed.name}</a>
						</td>
					</tr>
				</c:forEach>
			</table>			
		</c:if>
		<a class="btn btn-primary" href='<c:url value="feeds/add"/>'>Add Feed</a>
	</div>	
	
	<jsp:include page="../fragments/footer.jsp" />
	
</body>
</html>