<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>

<jsp:include page="../fragments/header.jsp" />

<body>
	<div class="container">
		<c:if test="${not empty msg}">
		    <div class="alert alert-${css} alert-dismissible" role="alert">
			<button type="button" class="close" data-dismiss="alert" aria-label="Close">
				<span aria-hidden="true">×</span>
			</button>
			<strong>${msg}</strong>
		    </div>
		</c:if>
	
		<h1>RSS Feed Details</h1>
		
		<div class="form-group row">
			<div class="col-sm-2 font-weight-bold col-form-label">Feed Name:</div>
			<div class="col-sm-4 form-control edit-disabled">${feed.name}</div>
		</div>
		<div class="form-group row">
			<div class="col-sm-2 font-weight-bold col-form-label">Feed URL:</div>
			<div class="col-sm-4 form-control edit-disabled">${feed.url}</div>
		</div>
		<div class="form-group row">
			<div class="col-sm-2 font-weight-bold col-form-label">Update Time:</div>
			<div class="col-sm-4 form-control edit-disabled">${feed.lastUpdate}</div>
		</div>
		<div class="form-group row">
			<div class="col-sm-2 font-weight-bold col-form-label">Article Count:</div>
			<div class="col-sm-4 form-control edit-disabled">${itemCount}</div>
		</div>
			
		<c:if test="${not empty recentItems}">
			<h2>Most Recent Articles</h2>
			<table class="table table-striped">
				<thead>
					<tr>
						<th>Article Title</th>
						<th>Article URL</th>
					</tr>
				</thead>
			
				<c:forEach var="item" items="${recentItems}">
					<tr>
						<td>${item.title}</td>
						<td>
							<a href="${item.link}">${item.link}</a>
						</td>
					</tr>
				</c:forEach>
			</table>			
		</c:if>
	</div>
<jsp:include page="../fragments/footer.jsp" />
	
</body>
</html>