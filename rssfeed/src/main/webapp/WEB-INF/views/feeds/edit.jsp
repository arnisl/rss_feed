<%@ page session="false"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<!DOCTYPE html>
<html>

<jsp:include page="../fragments/header.jsp" />

<body>
	<div class="container">
		<h1>Add XML RSS Feed</h1>
		<h2>Please provide new XML RSS Feed information</h2>
		<form:form method="POST" action="/feeds" modelAttribute="feed">
			<spring:bind path="url">
				<div class="form-group row">
					<form:label class="col-sm-2 col-form-label" path="url">XML RSS Feed URL:</form:label>
					<div class="col-sm-4">
						<form:input class="form-control ${status.error ? 'is-invalid' : ''}" path="url"/>
						<form:errors class="text-danger" path="url" />
					</div>
				</div>
			</spring:bind>
			
			<spring:bind path="name">
				<div class="form-group row">
					<form:label class="col-sm-2 col-form-label" path="name">XML RSS Feed Name:</form:label>
					<div class="col-sm-4">
						<form:input class="form-control ${status.error ? 'is-invalid' : ''}" path="name"/>
						<form:errors class="text-danger" path="name" />
					</div>	
				</div>
			</spring:bind>
			<button type="submit" class="btn btn-primary">Add Feed</button>
		</form:form>
	</div>
	
<jsp:include page="../fragments/footer.jsp" />	
	
</body>
</html>