<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<div class="container-fluid">
	<hr>
	<footer class="red-text text-center">
		<p>&copy; Arnis Lektauers 2018</p>
	</footer>
</div>

<script src="/webjars/jquery/3.3.1-1/jqyery.min.js"></script>
<script src="/webjars/popper.js/1.14.3/popper.min.js"></script>
