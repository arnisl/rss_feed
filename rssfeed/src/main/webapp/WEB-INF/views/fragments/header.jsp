<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<head>
	<title>XML RSS Feed Reader</title>
	 
	<link rel="stylesheet" href="/webjars/bootstrap/4.1.3/css/bootstrap.min.css" />
	<link rel="stylesheet" href="/webjars/font-awesome/5.2.0/css/all.min.css" />
	
	<link rel="stylesheet" href="<spring:url value="/resources/css/main.css" />"></link>
</head>

<spring:url value="/" var="urlHome"/>

<nav class="navbar navbar-expand-lg navbar-light">		
	<a class="navbar-brand" href="${urlHome}">
		<i class="fas fa-rss"></i>
		XML RSS Feed Reader
	</a>
</nav>