package com.pearl.rssfeed.dao;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.Optional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit4.SpringRunner;

import com.pearl.rssfeed.models.FeedItem;

@RunWith(SpringRunner.class)
@DataJpaTest
public class FeedItemDaoTest {
	
	@Autowired
    private TestEntityManager entityManager;

	@Autowired
    private FeedItemDao feedItemDao;
	
	@Test
	public void findById() {
		FeedItem feedItem = new FeedItem();
		feedItem.setDescription("Test Feed Item");

		entityManager.persist(feedItem);
	    entityManager.flush();
	    
	    Optional<FeedItem> found = feedItemDao.findById(feedItem.getId());
	   	    
	    assertThat(found.isPresent());	    
	    assertThat(found.get().getDescription()).isEqualTo(feedItem.getDescription());
	}
}
