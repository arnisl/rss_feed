package com.pearl.rssfeed.dao;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.Optional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit4.SpringRunner;

import com.pearl.rssfeed.models.Feed;

@RunWith(SpringRunner.class)
@DataJpaTest
public class FeedDaoTest {
	
	@Autowired
    private TestEntityManager entityManager;

	@Autowired
    private FeedDao feedDao;
	
	@Test
	public void findById() {
		Feed feed = new Feed();
		feed.setName("Test Feed");

		entityManager.persist(feed);
	    entityManager.flush();
	    
	    Optional<Feed> found = feedDao.findById(feed.getId());
	   	    
	    assertThat(found.isPresent());	    
	    assertThat(found.get().getName()).isEqualTo(feed.getName());
	}
}
