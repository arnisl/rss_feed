package com.pearl.rssfeed.controllers;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.model;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;

import java.util.Arrays;

import org.hamcrest.Matchers;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import com.pearl.rssfeed.controllers.RssFeedController;
import com.pearl.rssfeed.models.Feed;
import com.pearl.rssfeed.services.FeedItemService;
import com.pearl.rssfeed.services.FeedService;
import com.pearl.rssfeed.validators.FeedFormValidator;

@RunWith(SpringRunner.class)
@WebMvcTest(RssFeedController.class)
public class RssFeedControllerTest {

	@TestConfiguration
	static class RssFeedControllerTestConfiguration {
		@Bean
		public FeedFormValidator feedFormValidator() {
			return new FeedFormValidator();
		}
	}
	
	@Autowired
    private MockMvc mockMvc;
	
	@MockBean
    private FeedService feedService;

	@MockBean
    private FeedItemService feedItemService;

	@Test
	public void contextLoads() throws Exception {
		Feed feed = new Feed();
		feed.setName("Test feed");
		
		when(feedService.getFeeds()).thenReturn(Arrays.asList(feed));
		
		mockMvc.perform(get("/feeds")).andDo(print()).andExpect(status().isOk())
			.andExpect(view().name("feeds/list"))
			.andExpect(model().attribute("feeds", Matchers.hasItems(feed)));
	}
}
