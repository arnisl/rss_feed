package com.pearl.rssfeed.services;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.Optional;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.test.context.junit4.SpringRunner;

import com.pearl.rssfeed.dao.FeedDao;
import com.pearl.rssfeed.dao.FeedItemDao;
import com.pearl.rssfeed.models.Feed;

@RunWith(SpringRunner.class)
public class FeedServiceTest {

	@TestConfiguration
    static class FeedServiceTestContextConfiguration {
		
		@Bean
        public RssFeedParser feedParser() {
            return new RssFeedParser();
        }
  
        @Bean
        public FeedService feedService() {
            return new FeedServiceImpl();
        }
    }
	
	@MockBean
	private FeedItemDao feedItemDao;
 
	@MockBean
    private FeedDao feedDao;
	
    @Autowired
    private FeedService feedService;  
    
    @Before
    public void setUp() {
        Feed feed = new Feed();
        feed.setName("Test feed");
     
        Mockito.when(feedDao.findById(1)).thenReturn(Optional.of(feed));
    }
    
    @Test
    public void getFeedById() {	
    	Feed found = feedService.getFeedById(1);
    	assertThat(found.getName()).isEqualTo("Test feed");
    }
}
