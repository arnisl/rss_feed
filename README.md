# RSSFeed

XML RSS Feed Application supporting two main functionalities:

* Adding new XML RSS feeds.
* Viewing saved XML RSS feeds.

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.

### Prerequisites

* A favorite text editor or IDE
* [JDK 1.8](http://www.oracle.com/technetwork/java/javase/downloads/index.html)
* [Maven 3.2+](https://maven.apache.org/download.cgi)
* [MySQL 8.0+](https://dev.mysql.com/downloads/)
* [Lombok](https://projectlombok.org/)
* You can also import the code straight into your IDE:
	* [Spring Tool Suite (STS)](https://spring.io/guides/gs/sts)
	* [IntelliJ IDEA](https://spring.io/guides/gs/intellij-idea/)

## Deployment and Running

1. Install the MySQL 8.0+ server or use the existing one and import the MySQL dump file located in the "database" folder. 
2. Use the Maven build (in the folder "rssfeed") to create an application war file. The war will include all the necessary dependencies inside the archive including an embedded Tomcat server.
	* If you have Maven already installed use the following command: ``mvn clean install``
	* Or you can use the provided batch file mvnw.cmd with the following parameters: ``mvnw.cmd clean install``
3. Copy the provided db.properties configuration file to a folder where the war file is located.
4. Edit the db.properties file  parameters mysql.jdbcUrl, mysql.username, mysql.password corresponding to your database settings.
5. Start the created web application using the following command: ``java -jar rssfeed-1.0.war``
6. Start a browser at http://localhost:8080 to check the application.

## Built With

* [Spring MVC](https://docs.spring.io/spring/docs/current/spring-framework-reference/web.html) - The web framework used
* [Maven](https://maven.apache.org/) - Dependency Management

## Versioning

 Git (http://www.git.org) is used for versioning.

## Author

* **Arnis Lektauers** - [arnisl](https://bitbucket.org/arnisl)

